module Jekyll

  class PostsForTagPage < Page
      def initialize(site, base, dir, tag, posts)
        @site = site
        @base = base
        @dir = dir
        @name = "#{Utils.slugify(tag)}.html"

        self.process(@name)
        self.read_yaml(File.join(base, '_layouts'), 'posts-by-tag.html')
        self.data['posts'] = posts
        self.data['title'] = "Posts tagged '#{tag}'"
      end
  end

  class PostsForTagPageGenerator < Generator
    safe true

    def generate(site)
      tags_dir = site.config['tags_dir'] || 'tags'

      site.tags.each do |tag, pages|
        site.pages << PostsForTagPage.new(site, site.source, tags_dir, tag, pages)
      end
    end

  end

end